# kube-check-certificate

## Usage

```
Usage of ./kube-check-certificate:
  -n string
    	Namespace (default "shared")
  -s string
    	Secret
```

## Output

```
DNS Names: *.cloud.qlean.ru
Valid Not Before: 2021-08-01 04:26:07 +0000 UTC
Valid Not After: 2021-10-30 04:26:05 +0000 UTC
Valid: true
```
