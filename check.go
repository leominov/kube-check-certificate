package main

import (
	"context"
	"crypto/tls"
	"crypto/x509"
	"errors"
	"fmt"
	"strings"
	"time"

	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
)

const (
	crtSecretField = "tls.crt"
	keySecretField = "tls.key"
)

func ListCertificates(clientset kubernetes.Interface, namespace string) ([]*x509.Certificate, error) {
	secretList, err := clientset.CoreV1().Secrets(namespace).List(context.Background(), metav1.ListOptions{
		FieldSelector: "type=kubernetes.io/tls",
	})
	if err != nil {
		return nil, err
	}
	var certificates []*x509.Certificate
	for _, secret := range secretList.Items {
		certificate, err := parseSecretAsCertificate(&secret)
		if err != nil {
			return nil, err
		}
		certificates = append(certificates, certificate)
	}
	return certificates, nil
}

func CheckCertificate(clientset kubernetes.Interface, namespace, secretName string) (*x509.Certificate, error) {
	secret, err := clientset.CoreV1().Secrets(namespace).Get(context.Background(), secretName, metav1.GetOptions{})
	if err != nil {
		return nil, err
	}
	return parseSecretAsCertificate(secret)
}

func parseSecretAsCertificate(secret *corev1.Secret) (*x509.Certificate, error) {
	crt, ok := secret.Data[crtSecretField]
	if !ok {
		return nil, fmt.Errorf("%s not found in secret", crtSecretField)
	}
	key, ok := secret.Data[keySecretField]
	if !ok {
		return nil, fmt.Errorf("%s not found in secret", keySecretField)
	}
	cert, err := tls.X509KeyPair(crt, key)
	if err != nil {
		return nil, err
	}
	if len(cert.Certificate) == 0 {
		return nil, errors.New("empty certificate list")
	}
	return x509.ParseCertificate(cert.Certificate[0])
}

func PrintsCertificate(cert *x509.Certificate) {
	now := time.Now().UTC()
	fmt.Printf("DNS Names: %s\n", strings.Join(cert.DNSNames, ", "))
	fmt.Printf("Valid Not Before: %s\n", cert.NotBefore)
	fmt.Printf("Valid Not After: %s\n", cert.NotAfter)
	fmt.Printf("Valid: %v\n", now.After(cert.NotBefore) && now.Before(cert.NotAfter))
}
