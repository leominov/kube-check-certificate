package main

import (
	"flag"
	"fmt"
	"os"
)

var (
	namespace  = flag.String("n", "shared", "Namespace")
	secretName = flag.String("s", "", "Secret")
)

func main() {
	if err := realMain(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func realMain() error {
	flag.Parse()

	clientset, err := NewClientSet()
	if err != nil {
		return err
	}

	if *secretName != "" {
		cert, err := CheckCertificate(clientset, *namespace, *secretName)
		if err != nil {
			return err
		}
		PrintsCertificate(cert)
		return nil
	}

	certs, err := ListCertificates(clientset, *namespace)
	if err != nil {
		return err
	}
	for _, cert := range certs {
		PrintsCertificate(cert)
	}

	return nil
}
